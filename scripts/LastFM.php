<?php

/**
 * Last.FM api functionality
 * 
 * @author PC <paul.clarke+popex@holidayextras.com>
 * @date Fri Jan 20 14:50:19 GMT 2012
 */

class LastFM {
  var $username = 'PaulyPopEx';
  var $api_key = '40ae1f3b8203ee0235d24b86c07eaa04';
  var $email = 'pauly+lastfm@clarkeology.com';
  private $_session_key;
  private $_api_secret = 'c3ea2b4d37dddbd8a5129bf19eb393c9';
  private $_password = 'osdcontr0l lastfm';
  private $_url = 'http://ws.audioscrobbler.com/2.0/';

  public function session_key ( ) {
    if ( ! $this->_session_key ) {
      $json = $this->auth_getmobilesession( array( ));
      $this->_session_key = $json->session->key;
    }
    return $this->_session_key;
  }

  public function query ( $query = array( ), $sign = true ) {
    $query['api_key']= $this->api_key;
    if ( $sign ) {
      $query['username']= $this->username;
      $query['authToken'] = md5( $query['username'] . md5( $this->_password ));
    
      // Sign your authenticated calls by first ordering the parameters sent in your call alphabetically by parameter name
      // and concatenating them into one string using a <name><value> scheme. You must not include the format and callback
      // parameters. So for a call to auth.getSession you may have:
      // api_keyxxxxxxxxxxmethodauth.getSessiontokenyyyyyy
      // Ensure your parameters are utf8 encoded. Now append your secret to this string. Finally, generate an md5 hash of
      // the resulting string. For example, for an account with a secret equal to 'ilovecher', your api signature will be:
      // api signature = md5("api_keyxxxxxxxxxxmethodauth.getSessiontokenyyyyyyilovecher")
      // Where md5( ) is an md5 hashing operation and its argument is the string to be hashed. The hashing operation should
      // return a 32-character hexadecimal md5 hash.
      
      $signature = '';
      ksort( $query );
      foreach ( $query as $key => $val ) {
        if ( $key != 'format' and $key != 'callback' ) {
          $signature .= $key . $val;
        }
      }
      $signature .= $this->_api_secret;
      $query['api_sig'] = md5( $signature );
    }
    $query['format'] = 'json';
    if ( preg_match( '/\Wget/i', $query['method'] )) {
      $url = $this->_url . '?' . http_build_query( $query );
      return json_decode( $this->get( $url ));
    }
    $ch = curl_init( );
    // curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
    curl_setopt( $ch, CURLOPT_POST, 1 );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $query ));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_URL, $this->_url );
    curl_setopt( $ch, CURLOPT_USERAGENT, __FILE__ . '; ' . $this->email );
    $result = curl_exec( $ch );
    if ( curl_errno( $ch )) {
      error_log( __METHOD__ . ', ' . __LINE__ . ', ' . curl_error( $ch ));
    }
    curl_close( $ch );
    return json_decode( $result );
  }

  function get ( $url = '' ) {
    error_log( $url );
	  $file = '/tmp/' . preg_replace( '/\W+/', '+', $url );
	  $file = preg_replace( '/(api_key|authToken|api_sig)\+\w+/', '', $file );
	  $cache_hours = 24;
	  $result = '';
	  $filemtime = file_exists( $file ) ? filemtime( $file ) : 0;
	  if ( $filemtime and ((( time() - $filemtime ) / 3600 ) < $cache_hours ) and is_readable( $file )) {
		  $result = file_get_contents( $file );
	  }
	  else {	
      $ch = curl_init( );
      // curl_setopt( $ch, CURLOPT_GET, 1 );
      // curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
      curl_setopt( $ch, CURLOPT_URL, $url );
      curl_setopt( $ch, CURLOPT_USERAGENT, __FILE__ . '; ' . $this->email );
      $result = curl_exec( $ch );
      if ( curl_errno( $ch )) {
        error_log( __METHOD__ . ', ' . __LINE__ . ', ' . curl_error( $ch ));
      }
      curl_close( $ch );
		  @file_put_contents( $file, $result );
	  }
	  return $result;
  }

  /**
   * Missing method magic, like they do in rails...
   */
  public function __call( $name, $param ) {
    if ( preg_match( '/(\w+)_(\w+)/', $name, $m )) {
      $param[0]['method'] = $m[1] . '.' . $m[2];
      if ( $m[1] == 'user' and ! isset( $param[0]['user'] )) {
        $param[0]['user'] = $this->username;
      }
    }
    if ( isset( $param[1] )) {
      return $this->query( $param[0], $param[1] );
    }
    return $this->query( $param[0] );
  }
}

?>

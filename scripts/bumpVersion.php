<?php

$file = __DIR . '/../package.json';
$contents = file_get_contents($file);
$json = json_decode($contents);
$version = explode('.', $json->version);
$year = date('y');
$week = date('W');

$json->version = $version[0] . '.' . $year . '.' . $week;
file_put_contents($file, preg_replace('/    /', '  ', json_encode($json, JSON_PRETTY_PRINT)) . "\n");

?>

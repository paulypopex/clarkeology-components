<?php

function smarty_modifier_li ($tag = '', $folder = 'wiki', $title) {
  $tag = preg_replace('/^the\s+/i', '', $tag);
  $tag = preg_replace('/\s-\+\s.*$/i', '', $tag);
  $tag = preg_replace('/\.\s*/', ' ', $tag);
  $tag = preg_replace('/david devant.*/i', 'david devant', $tag);
  $tag = preg_replace('/mj hibbett.*/i', 'mj hibbett', $tag);
  $label = urldecode( $tag );
  $tag = trim( strtolower( $tag ));
  $url = '/wiki/#' . preg_replace('/^the\//', '', str_replace(' ', '/', $tag));
  if ( $tag == 'folkestone' ) $url = 'http://www.folkestonegerald.com/';
  if ( $tag == 'genealogy' ) $url = 'http://www.clarkeology.com/';
  if ( $tag == 'gigography' ) $url = 'http://www.clarkeology.com/misc/gigography/';
  if ( $tag == 'gig history' ) $url = 'http://www.clarkeology.com/misc/gigography/';
  if ( $tag == 'clarke' ) $url = 'http://www.clarkeology.com/names/clarke/';
  if ( $tag == 'paul clarke' ) $url = 'http://www.clarkeology.com/names/clarke/7/paul+leslie';
  if ( $tag == 'paul leslie clarke' ) $url = 'http://www.clarkeology.com/names/clarke/7/paul+leslie';
  if ( $tag == 'my blog' ) $url = 'http://www.clarkeology.com/blog/';
  if ( $tag == 'pauly' ) $url = 'http://www.clarkeology.com/blog/';
  if ( $tag == 'blog' ) $url = 'http://www.clarkeology.com/blog/';
  if ( $tag == 'folkestone gerald' ) $url = 'http://www.folkestonegerald.com/';
  if ( $tag == 'princes parade' ) $url = 'http://saveprincesparade.org';
  if (preg_match('/m.+tley cr.+e/i', $tag)) $url = '/wiki/#motley/crue';
  if ( $tag == 'guns n\' roses' ) $url = '/wiki/#guns/n/roses';
  $html = '<li><a rel="tag" href="' . $url . '"';
  if ($title) {
    $html .= ' title="' . htmlspecialchars($title) . '"';
  }
  $html .= '>' . $label . '</a></li>';
  return $html;
}

?>
